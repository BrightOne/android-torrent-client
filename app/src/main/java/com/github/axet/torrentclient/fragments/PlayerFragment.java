package com.github.axet.torrentclient.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.axet.androidlibrary.services.FileProvider;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.torrentclient.R;
import com.github.axet.torrentclient.activities.MainActivity;
import com.github.axet.torrentclient.app.MainApplication;
import com.github.axet.torrentclient.app.TorrentPlayer;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.List;

import libtorrent.Libtorrent;

public class PlayerFragment extends Fragment implements MainActivity.TorrentFragmentInterface {
    View v;
    LinearLayoutManager layout;
    RecyclerView list;
    View download;
    View empty;
    Files files;
    String torrentName;
    long pendindBytesUpdate; // update every new byte
    long pendindBytesLengthUpdate;
    int pendindSelected;
    TorrentPlayer player;
    TorrentPlayer.Receiver playerReceiver;
    ImageView play;
    View prev;
    View next;
    TextView playerPos;
    TextView playerDur;
    SeekBar seek;
    Handler handler = new Handler();

    public static void openIntent(Context context, Uri uri, String type, String name) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, type);
        FileProvider.grantPermissions(context, intent, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        context.startActivity(intent);
    }

    public static void shareIntent(Context context, Uri uri, String type, String name) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType(type);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.putExtra(Intent.EXTRA_EMAIL, "");
        intent.putExtra(Intent.EXTRA_SUBJECT, name);
        intent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.shared_via, context.getString(R.string.app_name)));
        FileProvider.grantPermissions(context, intent, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        context.startActivity(intent);
    }

    public static class FileHolder extends RecyclerView.ViewHolder {
        public TextView percent;
        public TextView size;
        public TextView folder;
        public TextView file;
        public TextView archive;
        public TextView archiveEnd;

        public FileHolder(View view) {
            super(view);
            percent = (TextView) view.findViewById(R.id.torrent_files_percent);
            size = (TextView) view.findViewById(R.id.torrent_files_size);
            folder = (TextView) view.findViewById(R.id.torrent_files_folder);
            file = (TextView) view.findViewById(R.id.torrent_files_name);
            archive = (TextView) view.findViewById(R.id.torrent_files_archive);
            archiveEnd = (TextView) view.findViewById(R.id.torrent_files_archive_end);
        }
    }

    public class Files extends RecyclerView.Adapter<FileHolder> {
        public int selected = -1;

        public String getFileType(int index) {
            TorrentPlayer.PlayerFile f = getItem(index);
            return TorrentPlayer.getType(f);
        }

        public void hide(int pos) {
            final long t = getArguments().getLong("torrent");
            TorrentPlayer.PlayerFile f = getItem(pos);
            Libtorrent.torrentFilesCheck(t, f.index, false);
            update();
        }

        public TorrentPlayer.PlayerFile getItem(int i) {
            return player.get(i);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            if (player == null)
                return 0;
            return player.getSize();
        }

        @Override
        public FileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View convertView = inflater.inflate(R.layout.torrent_player_item, parent, false);
            return new FileHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final FileHolder h, int position) {
            h.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    files.selected = h.getAdapterPosition();
                    files.notifyDataSetChanged();
                    playUpdate();
                }
            });
            h.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    PopupMenu popup = new PopupMenu(getContext(), h.itemView);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.menu_player, popup.getMenu());
                    final int pos = h.getAdapterPosition(); // view can be removed after refresh
                    final TorrentPlayer.PlayerFile f = files.getItem(pos);
                    MenuItem hide = popup.getMenu().findItem(R.id.action_hide);
                    if (f.arch != null)
                        hide.setVisible(false);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            String type = TorrentPlayer.getType(f);
                            int id = item.getItemId();
                            if (id == R.id.action_open) {
                                openIntent(getContext(), f.uri, type, f.getName());
                                return true;
                            }
                            if (id == R.id.action_share) {
                                shareIntent(getContext(), f.uri, type, f.getName());
                                return true;
                            }
                            if (id == R.id.action_hide) {
                                files.hide(pos);
                                return true;
                            }
                            return false;
                        }
                    });
                    popup.show();
                    return true;
                }
            });

            if (player != null && player.getPlaying() == position) {
                h.itemView.setBackgroundColor(ThemeUtils.getThemeColor(getContext(), R.attr.player_selection));
            } else {
                if (selected == position) {
                    h.itemView.setBackgroundColor(Color.LTGRAY);
                } else {
                    h.itemView.setBackgroundColor(Color.TRANSPARENT);
                }
            }

            final TorrentPlayer.PlayerFile f = getItem(position);

            h.percent.setEnabled(false);
            if (!f.isLoaded())
                MainApplication.setTextNA(h.percent, f.getPercent() + "%");
            else
                MainApplication.setTextNA(h.percent, "100%");

            h.size.setText(MainApplication.formatSize(getContext(), f.getLength()));

            if (f.arch != null && f.index == 0) {
                h.archive.setVisibility(View.VISIBLE);
                h.archive.setText(FilenameUtils.getExtension(f.tor.file.getPath()));
            } else {
                h.archive.setVisibility(View.GONE);
            }
            if (f.arch != null && f.index == (f.count - 1)) {
                h.archiveEnd.setVisibility(View.VISIBLE);
                h.archiveEnd.setText(FilenameUtils.getExtension(f.tor.file.getPath()));
            } else {
                h.archiveEnd.setVisibility(View.GONE);
            }

            String s = f.getPath();

            List<String> ss = splitPathFilter(s);

            if (ss.size() == 0) {
                h.folder.setVisibility(View.GONE);
                h.file.setText("./" + s);
            } else {
                if (position == 0) {
                    File p1 = new File(makePath(ss)).getParentFile();
                    if (p1 != null) {
                        h.folder.setText("./" + p1.getPath());
                        h.folder.setVisibility(View.VISIBLE);
                    } else {
                        h.folder.setVisibility(View.GONE);
                    }
                } else {
                    File p1 = new File(makePath(ss)).getParentFile();
                    String s2 = getItem(position - 1).getPath();
                    List<String> ss2 = splitPathFilter(s2);
                    File p2 = new File(makePath(ss2)).getParentFile();
                    if (p1 == null || p1.equals(p2)) {
                        h.folder.setVisibility(View.GONE);
                    } else {
                        h.folder.setText("./" + p1.getPath());
                        h.folder.setVisibility(View.VISIBLE);
                    }
                }
                h.file.setText("./" + ss.get(ss.size() - 1));
            }
        }
    }

    public static String makePath(List<String> ss) {
        if (ss.size() == 0)
            return "/";
        return TextUtils.join(File.separator, ss);
    }

    public List<String> splitPathFilter(String s) {
        List<String> ss = MainApplication.splitPath(s);
        if (ss.get(0).equals(torrentName))
            ss.remove(0);
        return ss;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.torrent_player, container, false);

        final long t = getArguments().getLong("torrent");

        empty = v.findViewById(R.id.torrent_files_empty);

        final MainApplication app = ((MainApplication) getContext().getApplicationContext());

        next = v.findViewById(R.id.player_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player == null)
                    return; // not yet open, no metadata
                int i = player.getPlaying() + 1;
                if (i >= player.getSize())
                    i = 0;
                play(i);
                files.selected = -1;
                files.notifyDataSetChanged();
                playUpdate(true);
                list.smoothScrollToPosition(i);
            }
        });
        prev = v.findViewById(R.id.player_prev);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player == null)
                    return; // not yet open, no metadata
                int i = player.getPlaying();
                if (i == -1) {
                    i = 0;
                } else {
                    i = i - 1;
                }
                if (i < 0)
                    i = player.getSize() - 1;
                play(i);
                files.selected = -1;
                files.notifyDataSetChanged();
                playUpdate(true);
                list.smoothScrollToPosition(i);
            }
        });
        play = (ImageView) v.findViewById(R.id.player_play);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = files.selected;
                if (index != -1) {
                    String type = files.getFileType(index);
                    if (!TorrentPlayer.isSupported(type)) {
                        TorrentPlayer.PlayerFile file = files.getItem(index);
                        openIntent(getContext(), file.uri, type, file.getName());
                        return;
                    }
                }
                if (app.player != null && app.player != player) {
                    boolean p = app.player.isPlaying();
                    app.playerClose();
                    if (p) { // if were playing show play button; else start playing current
                        app.playerStop();
                        playUpdate(false);
                        return;
                    }
                } // no else
                if (player.getPlaying() == -1 && files.selected == -1) { // start playing with no selection
                    play(0);
                    playUpdate(true);
                } else if (player.isPlaying() || player.getPlaying() == files.selected || files.selected == -1) { // pause action
                    int i = player.getPlaying();
                    player.pause();
                    if (player.isStop()) { // we stoped 'next' loop, keep last item highligted
                        files.selected = i;
                        files.notifyDataSetChanged();
                        playUpdate(false);
                    } else {
                        if (player.isPlaying()) { // did we resume?
                            files.selected = -1; // clear user selection after resume
                            files.notifyDataSetChanged();
                        }
                        playUpdate(true);
                    }
                    MainApplication app = ((MainApplication) getContext().getApplicationContext());
                    TorrentPlayer.save(getContext(), app.player);
                } else { // play selected file
                    play(index);
                    files.selected = -1;
                    files.notifyDataSetChanged();
                    playUpdate(true);
                }
            }
        });
        seek = (SeekBar) v.findViewById(R.id.player_seek);
        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    if (app.player == null)
                        return;
                    app.player.seek(progress);
                    app.player.notifyProgress();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        playerPos = (TextView) v.findViewById(R.id.player_pos);
        playerDur = (TextView) v.findViewById(R.id.player_dur);

        download = v.findViewById(R.id.torrent_files_metadata);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Libtorrent.downloadMetadata(t)) {
                    ((MainActivity) getActivity().getApplicationContext()).Error(Libtorrent.error());
                    return;
                }
            }
        });

        list = (RecyclerView) v.findViewById(R.id.list);

        files = new Files();

        DividerItemDecoration div = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        list.addItemDecoration(div);
        layout = new LinearLayoutManager(getContext());
        list.setLayoutManager(layout);
        list.setAdapter(files);

        playerPos.setText(MainApplication.formatDuration(getContext(), 0));
        playerDur.setText(MainApplication.formatDuration(getContext(), 0));

        playerReceiver = new TorrentPlayer.Receiver(getContext()) {
            @Override
            public void onReceive(Context context, Intent intent) {
                String a = intent.getAction();
                if (a.equals(TorrentPlayer.PLAYER_NEXT)) {
                    playUpdate(true);
                    files.notifyDataSetChanged();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (player != null && app.player == player) {
                                list.smoothScrollToPosition(player.getPlaying());
                            }
                        }
                    });
                }
                if (a.equals(TorrentPlayer.PLAYER_STOP)) {
                    playUpdate(false);
                    playerPos.setText(MainApplication.formatDuration(context, 0));
                    playerDur.setText(MainApplication.formatDuration(context, 0));
                    files.notifyDataSetChanged();
                }
                if (a.equals(TorrentPlayer.PLAYER_PROGRESS)) {
                    long pos = intent.getLongExtra("pos", 0);
                    long dur = intent.getLongExtra("dur", 0);
                    boolean p = intent.getBooleanExtra("play", false);
                    playerPos.setText(MainApplication.formatDuration(context, pos));
                    playerDur.setText(MainApplication.formatDuration(context, dur));
                    playUpdate(p);
                    seek.setMax((int) dur);
                    seek.setProgress((int) pos);
                }
            }
        };

        if (app.player != null)
            app.player.notifyProgress(playerReceiver);

        update();

        if (player != null) {
            int i = player.getPlaying() - 1;
            if (i < 0)
                i = 0;
            layout.scrollToPositionWithOffset(i, 0); // make 1 above
        }

        return v;
    }

    public void openPlayer(long t) {
        MainApplication app = ((MainApplication) getContext().getApplicationContext());
        if (player != null) {
            if (player == app.player)
                app.player = null;
            player.close();
            player = null;
        }
        if (app.player != null) {
            if (app.player.getTorrent() == t) {
                player = app.player;
                player.notifyProgress();
                return;
            }
        }
        player = new TorrentPlayer(getContext(), app.getStorage(), t);
        updatePlayer();
    }

    public void play(int i) {
        MainApplication app = ((MainApplication) getContext().getApplicationContext());
        if (app.player != null) {
            if (app.player != player) {
                app.player.close();
            }
        }
        app.player = player;
        player.play(i);
    }

    int getSelectedCount(long t) {
        int count = 0;
        long l = Libtorrent.torrentFilesCount(t);
        for (long i = 0; i < l; i++) {
            TorrentPlayer.TorFile f = new TorrentPlayer.TorFile(i, Libtorrent.torrentFiles(t, i));
            if (f.file.getCheck())
                count++;
        }
        return count;
    }

    @Override
    public void update() {
        long t = getArguments().getLong("torrent");

        if (Libtorrent.metaTorrent(t)) {
            empty.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
        }

        if (Libtorrent.metaTorrent(t)) {
            if (player == null || player.getTorrent() != t) {
                openPlayer(t);
                files.notifyDataSetChanged();
            } else {
                long p = Libtorrent.torrentPendingBytesCompleted(t);
                long pp = Libtorrent.torrentPendingBytesLength(t); // may not changed if files is zero length or part of another peace
                int ppp = getSelectedCount(t);
                boolean d = pendindBytesUpdate != p; // downloading
                boolean u = pendindBytesLengthUpdate != pp; // user selected
                boolean c = pendindSelected != ppp;
                if (d || u || c) {
                    player.update();
                    pendindBytesUpdate = p;
                    pendindBytesLengthUpdate = pp;
                    pendindSelected = ppp;
                    if (u) {
                        int i = player.getPlaying();
                        if (i > 0)
                            list.smoothScrollToPosition(i);
                    }
                    files.notifyDataSetChanged();
                }
            }
        }
        torrentName = Libtorrent.torrentName(t);
        updatePlayer();
    }

    void updatePlayer() {
        int i = player != null ? View.VISIBLE : View.GONE;
        play.setVisibility(i);
        prev.setVisibility(i);
        next.setVisibility(i);
        seek.setVisibility(i);
        playerPos.setVisibility(i);
        playerDur.setVisibility(i);
    }

    @Override
    public void close() {
        if (playerReceiver != null) {
            playerReceiver.close();
            playerReceiver = null;
        }
        MainApplication app = ((MainApplication) getContext().getApplicationContext());
        if (player != null) {
            if (player == app.player) {
                ; // then it is playing. do nothing.
            } else {
                player.close();
            }
            player = null;
        }
    }

    void playUpdate() {
        boolean playing = false;
        final MainApplication app = ((MainApplication) getContext().getApplicationContext());
        if (app.player != null && app.player != player) {
            playing = app.player.isPlayingSound();
        } else if (player != null) {
            playing = player.isPlayingSound();
        }
        playUpdate(playing);
    }

    void playUpdate(boolean playing) {
        if (player != null) {
            int index = files.selected;
            if (index == -1) {
                index = player.getPlaying();
            }
            if (index != -1) {
                String type = files.getFileType(index);
                if (TorrentPlayer.isSupported(type)) {
                    if (playing)
                        play.setImageResource(R.drawable.ic_pause_24dp);
                    else
                        play.setImageResource(R.drawable.play);
                } else {
                    play.setImageResource(R.drawable.ic_open_in_new_black_24dp);
                }
            } else {
                if (playing)
                    play.setImageResource(R.drawable.ic_pause_24dp);
                else
                    play.setImageResource(R.drawable.play);
            }
        } else {
            play.setImageResource(R.drawable.play);
        }
    }
}
